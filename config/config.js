"use strict";

const fs = require('fs');
const path = require('path');
const isProd = process.env.NODE_ENV === 'production';
const commonConfigPath = path.join(__dirname, './common.json');
const sensitiveConfigPath = path.join(__dirname, `./${isProd ? 'production' : 'develop'}/sensitive.json`);
const parsedCommon = JSON.parse(fs.readFileSync(commonConfigPath, 'UTF-8'));
const parsedSensitive = JSON.parse(fs.readFileSync(sensitiveConfigPath, 'UTF-8'));

module.exports = Object.assign({}, parsedCommon, parsedSensitive);
module.exports.isProd = isProd;
module.exports.prod = function () {
    const configPath = path.join(__dirname, `./production/sensitive.json`);
    return JSON.parse(fs.readFileSync(configPath, 'UTF-8'));
};
