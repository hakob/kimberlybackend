function pick(obj, props, excludeEmpty = false) {
    props = props || [];

    const r = {};

    for (const p of props) {
        if (excludeEmpty && typeof obj[p] === 'undefined') {
            continue;
        }
        r[p] = obj[p];
    }

    return r;
}

function picker(props) {
    return (obj) => pick(obj, props);
}

module.exports = {
    pick,
    picker
};