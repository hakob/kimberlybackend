"use strict";

const elasticSearch = require('elasticsearch');

class ElasticDB {
    static get instance() {
        return ElasticDB._instance;
    }

    static set instance(i) {
        return ElasticDB._instance = i;
    }


    static get uri() {
        return ElasticDB._uri;
    }

    static set uri(u) {
        return ElasticDB._uri = u;
    }

    static configure(host = 'localhost', port = 9200) {
        ElasticDB.uri = host + ':' + port;
    }

    constructor() {
        if (this.instance) {
            return this.instance
        }

        this.client = new elasticSearch.Client({
            host: ElasticDB.uri,
            log: 'error'
        });

        this.instance = this;
    }

    static isLive(cb) {
        const el = new ElasticDB();
        el.client.ping({requestTimeout: 1000}, (error) => {
            if (error) {
                console.




                trace('elasticsearch cluster is down!');
                return cb(error, false);
            }

            cb(null, true);
    });
    }

    get session() {
        return this.client;
    }
}

module.exports = ElasticDB;
