const util = require('util');

const errors = {
    1: 'Parameter not found in request: ',
    100: 'User Already Exists.',
    101: 'Invalid email.',
    102: 'Invalid user type.',
    103: 'Invalid username or password, please try again.',
    104: 'Can\'t save user.', //*
    106: 'Can\'t verify user.', //*
    107: 'We’re sorry but our database did not find an account with this email address.',
    108: 'Can\'t reset password.', //*
    109: 'Can\'t create verification url.', //*
    110: 'Can\'t reset user password.', //*
    111: 'Password reset token is invalid or has expired.',
    112: 'Wrong token.',
    114: 'User is not authenticated.',
    115: 'You are blocked. Please contact Solaron support.',
    116: 'User is not admin',
    117: 'User not checked',
    118: 'User blocked',
    119: 'Aggregation error',
    120: 'Station add error',
    121: 'Inverter add error',
    122: 'Station not found',
    123: 'Inverter already exists',
    124: 'User add error',
    125: 'Station with same name already exists',
    126: 'Inverter update error',
    127: 'Inverter remove error',
    128: 'Station update error',
    129: 'Station remove error',
    130: 'Inverter not found',
    131: 'Query error',
    132: 'Excel generation error',
    133: 'Global config error',
};

errors.noError = function (message, data) {
    let d = data, m = message;

    if (arguments.length === 1 && typeof arguments[0] === 'object') {
        d = message;
        m = null;
    }

    return {
        status: 'OK',
        message: m || 'Success',
        data: d
    }
};

errors.get = function (code, errs) {
    const error = {
        'code': code,
        'status': 'error',
        'message': errors[code]
    };

    if (errs) {
        util.log('Backedn error occurred: Error code ' + code);
        util.log(errs);
        util.log('------------------------------------')
    }

    if (errs) {
        if (Array.isArray(errs)) {
            error.errors = errs;
        } else if (typeof errs === 'object') {
            error.errors = process(errs);
        }
    }

    return error;
};

errors.response = (err, data, code, suberror=false) =>{
    if(err){
        return errors.get(code, suberror && [err]);
    }

    return errors.noError(data);
};

errors.missingParameter =(p)=>{
    return {
        'code': 1,
        'status': 'error',
        'message': errors[1] + p
    }
};

function process(errors) {
    if (!errors) {
        return;
    }

    return Object.keys(errors).map(function (prop) {
        return {
            'property': prop,
            'message': (errors[prop] || {}).message || errors[prop]
        }
    });
}

module.exports = errors;