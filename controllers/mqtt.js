"use strict";
const mosca = require('mosca');
const configs = require('../config/config');

const ascoltatore = {
    type: 'redis',
    redis: require('redis'),
    db: 12,
    port: 6379,
    host: configs.redis.chat.host,
    return_buffers: true
};

// simplest subscribe interface
const topicListeners = {};
const listenTopic = (topic, cb) => {
    console.log(topic +'55');
    topicListeners[topic] = topicListeners[topic] || [];
    topicListeners[topic].push(cb);
};

const triggerTopic = (topic, data) => {
    topicListeners[topic] = topicListeners[topic] || [];
    topicListeners[topic].forEach(c => c(data));
};

// create singleton server;
let server;

const authenticate = function (client, username, password, callback) {
    const authorized = username.toString() === configs.mqtt.username && password.toString() === configs.mqtt.password;
    if (authorized) {
        client.user = username
    }
    callback(null, authorized);
};

const authorizePublish = function (client, topic, payload, callback) {
    callback(null, client.user === topic.split('/')[1]);
};

const authorizeSubscribe = function (client, topic, callback) {
    callback(null, client.user === topic.split('/')[1]);
};

const broadcast = function (topic, payload, cb) {
    server.publish({topic, payload: JSON.stringify(payload), retain:true}, cb);
};

function createServer(port) {
    if (server) {
        return;
    }

    const moscaSettings = {
        host: "0.0.0.0",
        port: 1883 || port,
        backend: ascoltatore,
        persistence: {
            factory: mosca.persistence.Redis
        }
    };


    server = new mosca.Server(moscaSettings);
    server.on('ready', setup);

    server.on('clientConnected', function (client) {
        console.log('client connected', client.id);
    });

    // fired when a message is received
    server.on('published', function (packet) {
        triggerTopic(packet.topic, packet.payload);
        console.log(packet.topic, packet.payload.toString())
    });

    // fired when the mqtt server is ready
    function setup() {
        server.authenticate = authenticate;
        // server.authorizePublish = authorizePublish;
        // server.authorizeSubscribe = authorizeSubscribe;
        console.log('Mosca server is up and running')
    }
}

module.exports = {
    start: createServer,
    listenTopic,
    triggerTopic,
    broadcast
};
