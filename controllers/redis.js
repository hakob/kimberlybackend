const redis = require("redis");

const host = require('../config/config').redis.chat;
const redisClient = redis.createClient({
    host: host.host,
    port: host.port
});

const promiser = (res, rej) => {
    return function (err, resp) {
        if (err) {
            return rej(err);
        }

        res(resp);
    }
};

async function get(key) {
    return new Promise((res, rej) => {
        redisClient.get(key, promiser(res, rej));
});
}

async function del(key) {
    return new Promise((res, rej) => {
        redisClient.del(key, promiser(res, rej));
});
}

async function expire(key, seconds) {
    return new Promise((res, rej) => {
        redisClient.expire(key, seconds, promiser(res, rej));
});
}

async function set(key, data) {
    return new Promise((res, rej) => {
        redisClient.set(key, data, promiser(res, rej));
});
}

class Cache {
    constructor(key, source, expire) {
        this.key = key;
        this.expire = expire;
        this.source = source;
    }

    async set(data) {
        await set(this.key, data);
        await expire(this.key, this.expire);
    }

    async get() {
        let res = await get(this.key);

        if (!res) {
            res = await this.source();
            await this.set(res);
        }

        return res;
    }
}

module.exports = {
    set, get, del, Cache
};