"use strict";

const mqtt = require('./mqtt');
const elasticDb = require('./elastic');
const Validator = require('validatorjs');
const moment = require('moment');
const {MqttConfig, ConfigStatus} = require('../models/mqtt-config');
const {UpdateConfig} = require('../models/update-config');
const uuid = require('uuid');
const {pick} = require('../controllers/core');
const errors = require('../controllers/errors');
const elastic = new elasticDb();
const sensorMapping = {
    "kimberly": {
        "properties": {

            "id": {"type": "keyword"},

            "header": {
                "type": "object",
                "properties": {
                    "type": {"type": "long"},	// Monitoring Message Type
                    "ver": {"type": "long"},	// Message Format Version
                    "cntr": {"type": "long"},	// Message Counter
                    "utc": {"type": "long"}	// UTC time in epoch format
                },
            },
            "idrw_info": {
                "type": "object",
                "properties": {
                    "idrw_id": {"type": "text"},	// ID of UHF RFID Reader
                    "hw_ver": {"type": "text"},	// Hardware Version
                    "sw_ver": {"type": "text"},	// Software Version
                    "sett_ver": {"type": "text"},	// Settings Format Version
                    "region": {"type": "text"}	// Current region

                },
            },
            "tags": {
                "type": "nested",
                "properties": {
                    "ID": {"type": "text"},
                    "RSSI": {"type": "short"},
                }
            },
            "time": {"type": "date"},
            "time_received": {"type": "long"},
            "received": {"type": "date",}
        }

    }
};
const validationRules = {
    "id": "required",
    "header": "required",
    "idrw_info": "required",
    "tags": "array",
    "time": "required",
    "time_received": "required",
    "received": "required"
};

function transform(data) {
    const result = {id: data.idrw_info.idrw_id, header: data.header, idrw_info: data.idrw_info};
    const d = data.data;
    const unpacked = [];


    // unpack arrays
    for (let k in d) {
        if (d.hasOwnProperty(k) && k.slice(0, 4) === 'tag_') {
            unpacked.push(d[k])
        }
    }

    console.log(unpacked);
    // delete data.data;
    Object.assign(result, {tags: unpacked});

    result.received = Date.now();
    result.time_received = data.header.utc;

    const mTime = moment(result.header.utc * 1000);
    result.time = mTime.isValid() ? mTime.toDate() : result.received;


    if (!mTime.isValid()) {
        console.error("Wrong received time", data.time_received);
    }


    return result;
}

function validate(data) {
    const validation = new Validator(data, validationRules);

    if (validation.fails()) {
        console.log("Validation error: ", JSON.stringify(validation.errors.all()));

        return false;
    }

    return true;
}

const index = "sensor";
const mapping = "kimberly";
const imParams = Object.assign({}, {index: index}, {type: mapping});

// check
function checkAndCreate() {
    elastic.session.indices.exists({index: index}).then(exists => {
        if (!exists) {
            return elastic.session.indices.create({index: index});
        }

        return true;
    }).then(_ => {
        return elastic.session.indices.existsType(imParams);
    }).then(exists => {
        if (!exists) {
            return elastic.session.indices.putMapping(Object.assign({}, imParams, {body: sensorMapping}))
        }

        return true;
    }).then(_ => {
        console.log("DB successfully initialized");
        listenMQTT();
    }).catch(err => {
        console.error("Error from DB", JSON.stringify(err))
    });
}

const sample = {
    "kimberly": {
        "properties": {
            "id": {"type": "keyword"},
            "header": {
                "type": {"type": "short"},	// Monitoring Message Type
                "ver": {"type": "short"},	// Message Format Version
                "cntr": {"type": "int"},	// Message Counter
                "utc": {"type": "long"}	// UTC time in epoch format
            },
            "idrw_info": {
                "idrw_id": {"type": "text"},	// ID of UHF RFID Reader
                "hw_ver": {"type": "text"},	// Hardware Version
                "sw_ver": {"type": "text"},	// Software Version
                "sett_ver": {"type": "text"},	// Settings Format Version
                "region": {"type": "text"}	// Current region

            },
            "data": {
                "#tags": {"type": "short"},	// number of ID Tags discovered


                "tag-1": {
                    "ID": {"type": "text"},	// ID of 1-st Tag
                    "RSSI": {"type": "text"},		// RSSI of 1-st Tag read
                },
                "tag-2": {
                    "ID": {"type": "text"},	// ID of 2-nd Tag
                    "RSSI": {"type": "text"},		// RSSI of 2-nd Tag read
                },


                "tag-#": {
                    "ID": {"type": "text"},	// ID of #-th Tag
                    "RSSI": {"type": "text"},		// RSSI of #-th Tag read
                }
            }

        }
    }
};

function randomizer(t) {
    switch (t) {
        case 'float':
            return Math.random() * 5;
        case 'short':
            return Math.round(Math.random() * 10);
        case 'string':
            return uuid.v4();
        case 'date':
            return new Date();

    }
}

function randomData() {
    const r = {};

    for (const [key, value] of Object.entries(sample)) {
        if (key === 'ip') {
            r['ip'] = '123.45.607.89'
        }
        else if (key === 'id') {
            r['id'] = 'SecondBestSerialNumber'
        }
        else {
            r[key] = randomizer(value.type);
        }
    }

    return r;
}

//
// setInterval(function(){
//     const data = randomData();
//     if (!validate(data)) {
//         console.log('esh data');
//         return;
//     }
//     elastic.session.index(Object.assign({}, imParams, {body: data})).catch(err => {
//         console.error("Error from DB", JSON.stringify(err))
//     }).then(function () {
//         console.log('Cool');
//     })
// }, 100);


const configBroadcster = async function () {
    try {

        let config = await MqttConfig.findOne({status: ConfigStatus.LIVE});
         console.log(config)
        if (!config) {
            return;
        }

        const data = {
            "header": {"type": 0, "ver": config.buildVersion},
            "publish-intvl": config.publish_intvl,
            "rf-cfg": {
                "tx-pwr": config.tx_pwr,
                "dense-settings": {
                    "dense-mode": config.dense_mode,
                    "fh-table": config.fh_table,
                    "read-time": config.read_time,
                    "idle-time": config.idle_time,
                    "cs-time ": config.cs_time,
                    "target-pwr": config.target_pwr
                }
            }
        }


        mqtt.broadcast("/server/config", data, function () {
           // console.log("Config broadcast", arguments);
        });
    } catch (err) {
        console.log(err);
    }
};





const updateBroadcster = async function () {
    try {

        let config = await UpdateConfig.findOne({status: ConfigStatus.LIVE});

        if (!config || !config.source) {
            return;
        }

        const data ={
            header :{
                type: 0,
                ver: config.buildVersion
            },
            port:config.port,
            ip:config.ip,
            source:config.source

        }


        mqtt.broadcast("/server/update", data, function () {
            // console.log("Update broadcast", arguments);
        });
    } catch (err) {
        console.log(err);
    }
};

function listenMQTT() {
    mqtt.listenTopic('sensorData', function (data) {
        const d = data.toString();
        let json = null;

        try {
            json = JSON.parse(d);
            json = transform(json);
            json = pick(json, Object.keys(validationRules));
            console.info(json);
            if (!validate(json)) {
                return;
            }

        } catch (err) {
            console.error(Date.now(), "Unrecognized JSON", d);
            return;
        }

        elastic.session.index(Object.assign({}, imParams, {body: json})).catch(err => {
            console.error("Error from DB", JSON.stringify(err))
        })
    });

    setInterval(configBroadcster, 1000 * 60*5);
    configBroadcster().catch(e => {
        console.log(e);
    });

    setInterval(updateBroadcster, 1000 * 60*6);
    updateBroadcster().catch(e => {
        console.log(e);
    });
}


module.exports = {
    init: checkAndCreate,
    sensorDbParams: imParams,
    properties: sensorMapping.kimberly.properties
};


