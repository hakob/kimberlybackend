/**
 * Created by vahagn.
 */
"use strict";

const plan = require('flightplan');
const host = require('./config/config').prod().hosts[0];
const current = '/home/deployment/server';
const upload = '/home/deployment/deployment-uploads';
const path = require('path');
const fs = require('fs');

plan.target('production', [
    {
        host: host.host,
        username: host.username,
        password: host.password,
    }
]);

plan.remote([ 'restart' ], function (remote) {
    remote.log('Restart application');
    remote.exec('sudo systemctl restart nodeserver.service', {user: 'solaron'});
    remote.log('Application restarted successfully');
});

plan.remote([ 'setup' ], function (remote) {
    remote.log('Setup server...');
    remote.sudo('apt-get update', { user: 'solaron' });
    remote.sudo('apt-get install npm', { user: 'solaron' });
    remote.sudo('curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -', { user: 'solaron' });
    remote.sudo('apt-get install nodejs', { user: 'solaron' });
    remote.sudo('apt-get install build-essential', { user: 'solaron' });
});

// run commands on localhost
plan.local([ 'server-deploy' ], function (local) {
    const appName = 'server';
    local.log('Copy files to remote hosts');

    let remoteDir;

    local.exec('npm run patch');
    remoteDir = `/home/deployment/${appName}`;

    let filesToCopy = local.exec('find ./* -not -path "./node_modules/*" -not -path "./kimberly_uploads/*"', {silent: true});

    local.transfer(filesToCopy, remoteDir);
});

// run commands on the target's remote hosts
plan.remote([ 'server-deploy' ], function (remote) {
    remote.exec(`mkdir -p ${upload}`);
    remote.exec(`chown -R deployment ${upload}`, {failsafe: true});

    remote.log('Install dependencies');
    remote.with(`cd ${current}`, function () {
        remote.exec('npm install');
    });

    remote.log('Reload application');
    remote.with(`cd ${current}`, function () {
        remote.exec('pm2 delete kimberly', { failsafe: true });
        remote.exec('pm2 start index.js --name kimberly');
    });
    remote.log('Application deployed successfully');
});
