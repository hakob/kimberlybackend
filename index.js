const fs = require('fs');
const path = require('path');
const uuid = require('uuid');
const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const configs = require('./config/config');
const isProd = app.get('env') === 'production';
const port = configs.port;
const host = configs.hosts[0];
const db = configs.dbs[host.datacenter];
const Validator = require('validatorjs');
const {pick} = require('./controllers/core');
const allConfigs = require('./config/config');
const {MqttConfig, ConfigStatus} = require('./models/mqtt-config');
const {UpdateConfig} = require('./models/update-config');
const errors = require('./controllers/errors');
const elasticDb = require('./controllers/elastic');
const elastic = new elasticDb();


const ElasticDB = require('./controllers/elastic');
ElasticDB.configure(configs.elasticsearch.host);

const mqtt = require('./controllers/mqtt');
mqtt.start();

const sensor = require('./controllers/sensor');
const i = setTimeout(function () {
    ElasticDB.isLive(function (error, isLive) {
        if (isLive) {
            sensor.init();
            clearInterval(i);
        } else {
            console.log(JSON.stringify(error));
        }
    });
}, 1000);


if (isProd) {
    process.env.HOME = '/home/deployment';
}


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const index = "sensor";
const mapping = "kimberly";
const imParams = Object.assign({}, {index: index}, {type: mapping});

// api

async function getId() {

    const query = {
        body: {

            "size": 0,
            "aggs": {
                "hits": {
                    "terms": {"field": "id"}
                }
            }

        }
    };


    return new Promise(function (resolve, reject) {
        elastic.session.search(Object.assign({}, imParams, query)).then(function (resp) {

            resolve(resp);
            const data = resp
            // console.log(data)
        }).catch(function (err) {
            reject(err);
        })
    });
}

async function getTags(id) {
    const idFilter = {
        term: {
            "id": id
        }
    };
    const filters = [idFilter];
    const queryFilter = {bool: {must: filters}}
    const query = {
        body: {
            "query": {
                term: {
                    "id": id
                }
            },
            "sort": [{"received": "desc"}],
            "size": 100
        }
    };
    //console.log(query);

    return new Promise(function (resolve, reject) {
        elastic.session.search(Object.assign({}, imParams, query)).then(function (resp) {

            resolve(resp);
            const data = resp
            //  console.log(data.hits.hits)
        }).catch(function (err) {
            reject(err);
        })
    });
}


app.get('/api/home', async function (req, res) {

    try {
        const data = await getId();
        res.send(JSON.stringify(data.aggregations.hits.buckets));

    } catch (err) {
        res.send(errors.get(119, [err.message]));
    }
});


app.post('/api/tags', async function (req, res) {
    const body = req.body;
    const deviceId = body.id;
    try {
        const response = await getTags(deviceId);
        const documents = response.hits.hits;
        //console.log(recieved)
        const data = [];
        for (let doc of documents){
            if(doc._source.tags.length === 0){
                continue;
            }
            data.push(...doc._source.tags.map(t=>Object.assign({}, t, {time: doc._source.received})));
        }

        res.send(JSON.stringify(data));
    } catch (err) {
        res.send(errors.get(119, [err.message]));
    }
});


const MqttConfigAddRules = {
    region: 'required|string',
    publish_intvl:'required|numeric',
    tx_pwr: 'required|numeric',
    dense_mode: 'required|numeric',
    fh_table: 'required',
    cs_time: 'required|numeric',
    read_time: 'required|numeric',
    idle_time: 'required|numeric',
    target_pwr: 'required|numeric'
};

const UpdateConfigAddRules = {
    source: 'required|string'
};

app.post('/api/update', async function (req, res) {
    const body = req.body;

    try {
        const v = new Validator(body, UpdateConfigAddRules);

        if (v.fails()) {
            return res.send(errors.get(133, v.errors.all()));
        }

        const source = pick(body, Object.keys(UpdateConfigAddRules));
        source.ip = allConfigs.ip;
        source.port = allConfigs.port;
       const last = await UpdateConfig.findOne({status: ConfigStatus.LIVE}) || {};

        if (source.source && !source.source.startsWith("/")) {
            source.source = "/" + source.source;
        }

        source.buildVersion = (last.buildVersion || 0) + 1;
        const config = new UpdateConfig(source);
        await config.save();
        await UpdateConfig.update({_id: {$ne: config._id}}, {$set: {status: ConfigStatus.OLD}}, {multi: true});

        return res.send(errors.noError());

    } catch (err) {
        console.log(err);
        return res.send(errors.get(133, [err.message]));
    }
});

app.post('/api/config', async function (req, res) {
    const body = req.body;
    console.log(body)

    try {
        const v = new Validator(body, MqttConfigAddRules);
        const last = await MqttConfig.findOne({status: ConfigStatus.LIVE}) || {}

        if (v.fails()) {
            return res.send(errors.get(133, v.errors.all()));
        }
        const source = pick(body, Object.keys(MqttConfigAddRules));
        source.buildVersion = (last.buildVersion || 0) + 1;
        const config = new MqttConfig(source);
        await config.save();
        await MqttConfig.update({_id: {$ne: config._id}}, {$set: {status: ConfigStatus.OLD}}, {multi: true});

        return res.send(errors.noError());
    } catch (err) {
        console.log(err);
        return res.send(errors.get(133, [err.message]));
    }
});

//  fronmt end
const frontend = __dirname + '/kimberly-frontend';

if (!fs.existsSync(frontend)) {
    fs.mkdirSync(frontend);
}

app.use(express.static(frontend));
app.get('*', (req, res) => {
    res.sendfile(path.join(`${frontend}/index.html`));
});

const multer = require('multer');
const uploadsDest = __dirname + '/kimberly-uploads';
const patchesDest = uploadsDest + '/patches';

if (!fs.existsSync(uploadsDest)) {
    fs.mkdirSync(uploadsDest);
}

app.use(express.static(uploadsDest));

const patchUploader = multer({
    dest: patchesDest
});

app.post('/patch', patchUploader.single('qqfile'), function (req, res) {
    const newName = uuid.v4().replace(/-/g, ''),
        file = req.file;


    if (!file) {
        return res.send(errors.get(172));
    }

    const filePath = file.path;
    const newFullName = newName + path.extname(file.originalname);

    fs.rename(filePath, file.destination + '/' + newFullName, function (err) {
        const name = err ? file.filename : newFullName;
        return res.send({
            'success': true,
            'url': '/patches/' + name
        })
    });
});

// handle mongoose promises
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${db.host}:${db.port}/kimberly`, {}, a => {
    console.log(a ? 'error' : 'info', a || 'Connected to MongoDB')
});


const HOST = "0.0.0.0";
// run server
const server = app.listen(port, HOST, function () {
        console.log('HTTP sever is running');
    }
);
