const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConfigStatus = {
    OLD: 0,
    LIVE: 1
};

const MqttConfigSchema = new Schema({
    created: {
        type: 'Date',
    },
    status: {
        type: 'Number',
        required: true,
        default: ConfigStatus.LIVE
    },
    updated: {
        type: 'Date',
    },
    region: {
        type: 'String',
        required: true
    },
    publish_intvl: {
        type: 'number',
        required: true
    },


    tx_pwr: {
        type: 'number',
        required: true
    },
    dense_mode: {
        type: 'number',
        required: true
    },
    fh_table: {
        type: ['number'],
        required: true
    },
    cs_time: {
        type: 'number',
        required: true
    },
    read_time: {
        type: 'number',
        required: true
    },
    idle_time: {
        type: 'number',
        required: true
    },

    target_pwr: {
        type: 'number',
        required: true
    },
    buildVersion: {
        type: 'Number',
        required: true,
        default: 0
    }
});


MqttConfigSchema.pre('save', function (next) {
    const now = new Date();
    if (!this.created) {
        this.created = now;
    }

    if (!this.updated) {
        this.updated = now;
    }

    next();
});

const MqttConfig = mongoose.model('MqttConfig', MqttConfigSchema);

module.exports = {MqttConfig, ConfigStatus};
