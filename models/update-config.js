const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConfigStatus = {
    OLD: 0,
    LIVE: 1
};

const UpdateConfigSchema = new Schema({
    created: {
        type: 'Date',
    },
    status: {
        type: 'Number',
        required: true,
        default: ConfigStatus.LIVE
    },
    updated: {
        type: 'Date',
    },
    source: {
        type: 'String'
    },
    ip: {
        type: 'String',
        required: true
    },
    port: {
        type: 'Number',
        required: true
    },
    buildVersion: {
        type: 'Number',
        required: true,
        default: 0
    }
});


UpdateConfigSchema.pre('save', function (next) {
    const now = new Date();
    if (!this.created) {
        this.created = now;
    }

    if (!this.updated) {
        this.updated = now;
    }

    next();
});

const UpdateConfig = mongoose.model('UpdateConfig', UpdateConfigSchema);

module.exports = {UpdateConfig, ConfigStatus};
