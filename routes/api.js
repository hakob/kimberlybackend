/**
 * Created by vahagn.
 */
"use strict";

const Validator = require('validatorjs');
const allConfigs = require('../config/config');
const Station = require('../models/station');
const {MqttConfig, ConfigStatus} = require('../models/mqtt-config');
const {StationManager} = require('../controllers/data-manager');
const errors = require('../controllers/errors');
const UserController = require('../controllers/user');
const excel = require('../controllers/excel');
const elasticDb = require('../controllers/elastic');
const elastic = new elasticDb();
const {sensorDbParams, properties} = require('../controllers/sensor');
const {pick} = require('../controllers/core');

const StationAddRules = {
    name: 'required|string',
    country: 'required|string',
    city: 'required|string',
    state: 'required|string',
    address: 'string',
    location: 'array',
    capacity: 'required|numeric',
    compensatoryEnergy: 'numeric',
    currency: 'required|string',
    price: 'required|numeric',
    image: 'string',
};

const WIFIInverterAddRules = {
    serialNumber: 'required|string',
    checkCode: 'string',
    type: 'string',
    description: 'string',
    brand: 'string',
    model: 'string'
};

const MqttConfigAddRules = {
    interval: 'required|numeric',
    source: 'string',
    version: 'required|numeric',
    minorVersion: 'required|numeric',
    buildVersion: 'required|numeric',
};


function oneUpdatedCallback(cb, res) {
    return function (err, modified, ...params) {
        if (!err && (!modified || (Number.isInteger(modified.nModified) && modified.nModified !== 1))) {
            return res.send(errors.get(137));
        }

        cb(err, ...params)
    };
}

function $isInverterExists(serialNumber) {
    const query = {
        body: {
            "query": {"term": {"id": serialNumber}},
            "size": 0,
            "terminate_after": 1
        }
    };

    return new Promise(function (resolve, reject) {
        elastic.session.search(Object.assign({}, sensorDbParams, query)).then(function (resp) {
            resolve(resp.hits.total > 0);
        }).catch(function (err) {
            reject(err);
        })
    });
}

const bucketsMap = (h, prop) => h.map(b => b[prop].value || 0);
const histogramMap = (h, key, prop) => h.map(h => (h.series.buckets.find(s => s.key === key) || {[prop]: {}})[prop].value || 0);

function $inverterLastData(inverterId, interval = 5) {
    const idFilter = {
        term: {
            "id": inverterId
        }
    };

    const dateFilter = {
        "range": {
            "time": {
                "gte": `now-${interval * 2}m`,
            }
        }
    };

    const filters = [idFilter];

    if (interval) {
        filters.push(dateFilter);
    }

    const queryFilter = {bool: {must: filters}};

    const query = {
        body: {
            "query": queryFilter,
            "sort": [{"time": "desc"}],
            "size": 1
        }
    };

    //console.log(query);

    return new Promise(function (resolve, reject) {
        elastic.session.search(Object.assign({}, sensorDbParams, query)).then(function (resp) {
            resolve(resp.hits.hits[0] ? resp.hits.hits[0]._source : {});
        }).catch(function (err) {
            reject(err);
        })
    })
}

async function $okInverter(station, source) {
    const e = await $isInverterExists(source.serialNumber);
    if (!e) {
        throw errors.get(130);
    }

    const exists = station.wifiInverters.find(w => w.serialNumber === source.serialNumber);

    if (exists) {
        throw errors.get(123);
    }

    return true
}

function calculateInterval(start, end, points) {
    const diff = (end - start);
    let c = (parseInt(diff / (points * 1000 * 60)) || 1);

    // not less than five minute
    c = c < 5 ? 5 : c;

    // find nearly good interval, good intervals that is multiply by 60, e.g 1,2,3,5,10,20,30, 60 ...
    const good = [1,2,3,5,10,15,20, 30, 60];
    let st = 1, p =1, i=1;
    while(st<c){
        p = st;
        if(i<good.length){
            st = good[i++];
        } else {
            st*=2;
        }
    }

    c = p;

    return c;
}

async function getChartData(body) {
    const start = Date.parse(body.start) || (new Date() - 1000 * 60); //last hour
    const end = Date.parse(body.end) || new Date(); //last hour
    const stationId = body.stationId;
    const wifiInverters = body.inverters || [];
    const points = body.points || 30;

    if (!stationId) {
        throw errors.missingParameter('stationId');
    }

    const station = await Station.findById(stationId);

    if (!station) {
        throw errors.get(122);
    }

    let allInverters = station.wifiInverters.map(w => w.serialNumber);
    let inverters = allInverters.filter(i => wifiInverters.includes(i));

    const averageAll = inverters.length === 0;

    if (averageAll) {
        inverters = allInverters;
    }

    let prop = body.property || [];
    let props = [prop];

    if (prop.includes('pv-power')) {
        const index = prop[prop.length - 1];
        props = ['pv-voltage_' + index, 'pv-current_' + index];
    }

    let propsTransform = (data) => {
        if (prop === 'pv-power_0') {
            return data[0].map((d, i) => d * data[1][i]);
        }

        return data[0];
    };

    const idFilter = {
        terms: {
            id: inverters
        }
    };

    const dateFilter = {
        "range": {
            "time": {
                "gt": start,
                "lte": +end
            }
        }
    };

    const filters = [idFilter, dateFilter];
    const queryFilter = {bool: {must: filters}};

    const interval = calculateInterval(start, end, points);
    const dateHistogram = {
        field: 'time',
        interval: interval + 'm',
        min_doc_count: 0,
        extended_bounds: {
            "min": start,
            "max": end
        }
    };

    const aggs = props.reduce((r, p) => {
        r[p] = {avg: {field: p}};
        return r;
    }, {});

    let groupBy = {
        "series": {
            "terms": {
                "field": "id"
            },
            "aggs": aggs
        }
    };

    if (averageAll) {
        groupBy = aggs;
    }

    const query = {
        body: {
            size: 0,
            query: queryFilter,
            aggs: {
                "histogram": {
                    "date_histogram": dateHistogram,
                    aggs: groupBy
                }
            }
        }
    };

    return new Promise(function (resolve, reject) {
        elastic.session.search(Object.assign({}, sensorDbParams, query)).then(function (resp) {
            const histogram = resp.aggregations.histogram.buckets;
            const data = {
                time: histogram.map(b => b.key_as_string),
                series: averageAll ? ['All inverters'] : inverters,
            };

            let seriesData = averageAll ? [bucketsMap(histogram, prop)] :
                inverters.map(i => propsTransform(props.map(prop => histogramMap(histogram, i, prop))));

            data.data = seriesData;

            resolve(data);
        }).catch(function (err) {
            reject(err);
        })
    });
}

const ranges = {
    month: {range: ["now/M", "now"], interval: 'day'},
    year: {range: ["now/y", "now/y+1y-1s"], interval: 'month'},
    all: {range: [undefined, "now"], interval: 'year'}
};

async function getEnergyChartData(body) {
    let interval = body.interval;

    if (!ranges[interval]) {
        interval = 'year';
    }

    const stationId = body.stationId;
    const wifiInverters = body.inverters || [];

    if (!stationId) {
        throw errors.missingParameter('stationId');
    }

    const station = await Station.findById(stationId);

    if (!station) {
        throw errors.get(122);
    }

    let allInverters = station.wifiInverters.map(w => w.serialNumber);
    let inverters = allInverters.filter(i => wifiInverters.includes(i));

    const averageAll = inverters.length === 0;

    if (averageAll) {
        inverters = allInverters;
    }

    const idFilter = {
        terms: {
            id: inverters
        }
    };

    const dateRange = ranges[interval].range;
    const dateFilter = {
        "range": {
            "time": {
                "gte": dateRange[0],
                "lt": dateRange[1]
            }
        }
    };

    const filters = [idFilter, dateFilter];
    const queryFilter = {bool: {must: filters}};

    const dateHistogram = {
        field: 'time',
        interval: ranges[interval].interval,
        min_doc_count: 0,
        extended_bounds: {
            "min": dateRange[0],
            "max": dateRange[1]
        }
    };
    const aggs = {"energy": {max: {field: "energy-produced-during-the-day"}}};


    let groupBy = {
        "series": {
            "terms": {
                "field": "id"
            },
            "aggs": aggs
        }
    };

    if (averageAll) {
        groupBy = aggs;
    }

    const query = {
        body: {
            size: 0,
            query: queryFilter,
            aggs: {
                "histogram": {
                    "date_histogram": dateHistogram,
                    aggs: groupBy
                }
            }
        }
    };

    return new Promise(function (resolve, reject) {
        elastic.session.search(Object.assign({}, sensorDbParams, query)).then(function (resp) {
            const histogram = resp.aggregations.histogram.buckets;
            const prop = "energy";

            const data = {
                time: histogram.map(b => b.key_as_string),
                series: averageAll ? ['All inverters'] : inverters,
            };

            let seriesData = averageAll ? [bucketsMap(histogram, prop)] :
                inverters.map(i => histogramMap(histogram, i, prop));

            data.data = seriesData;

            resolve(data);
        }).catch(function (err) {
            reject(err);
        })
    });
}


const zip = rows => rows[0].map((_, c) => rows.map(row => row[c]));

module.exports = function (router) {
    router.post('/chart/data', async function (req, res) {
        try {
            const data = await getChartData(req.body);
            res.send(errors.noError(data));
        } catch (err) {
            res.send(errors.get(119, [err.message]));
        }
    });

    router.post('/chart/energy', async function (req, res) {
        try {
            const data = await getEnergyChartData(req.body);
            res.send(errors.noError(data));
        } catch (err) {
            res.send(errors.get(119, [err.message]));
        }
    });


    router.post('/chart/last', async function (req, res) {
        const body = req.body;
        const stationId = body.stationId;

        if (!stationId) {
            return res.send(errors.missingParameter('stationId'));
        }

        const station = await Station.findById(stationId);

        if (!station) {
            return res.send(errors.get(122));
        }

        const inverters = station.wifiInverters.map(w => w.serialNumber);
        const config = await MqttConfig.findOne({status: ConfigStatus.LIVE}) || {};
        const data = [];
        try {
            for (const i of inverters) {
                let last = await $inverterLastData(i, config.interval);
                let realTime = true;

                if (!last.id) {
                    // get last data
                    last = await $inverterLastData(i, null);
                    realTime = false;
                }

                data.push({inverterId: i, data: last, realTime});
            }

            res.send(errors.noError(data));
        } catch (err) {
            res.send(errors.get(119, [err.message]));
        }
    });

    router.post('/station/get', async function (req, res) {
        try {
            const body = req.body;
            const stationId = body.stationId;

            if (!stationId) {
                return res.send(errors.missingParameter('stationId'));
            }

            const station = await Station.findById(stationId);

            if (!station) {
                return res.send(errors.get(122));
            }

            return res.send(errors.noError(station))
        } catch (err) {
            return res.send(errors.get(120, [err.message]));
        }
    });

    router.post('/station/add', async function (req, res) {
        const body = req.body;
        const v = new Validator(body, StationAddRules);

        if (v.fails()) {
            return res.send(errors.get(120, v.errors.all()));
        }
        try {
            // check if invertor exists with given serialNumber
            const exists = await Station.findOne({name: body.name});

            if (exists) {
                return res.send(errors.get(125));
            }

            const source = pick(body, Object.keys(StationAddRules));
            source.user = req.user._id;
            const station = new Station(source);

            if (Array.isArray(body.inverters)) {
                for (const i of body.inverters) {
                    const inverter = pick(i, Object.keys(WIFIInverterAddRules));

                    try {
                        await $okInverter(station, inverter);
                    } catch (err) {
                        return res.send(err);
                    }

                    station.wifiInverters.push(inverter);
                }
            }

            await station.save();

            return res.send(errors.noError({stationId: station._id}));
        } catch (err) {
            return res.send(errors.get(120, [err.message]));
        }
    });

    router.post('/station/query', async function (req, res) {
        let p = req.body;

        p = Object.assign({}, p, req.query);

        const s = new StationManager(p);
        try {
            const result = await s.call();
            return res.send(result);
        } catch (err) {
            res.send(errors.get(131, [err.message]))
        }
    });

    router.post('/station/update', async function (req, res) {
        const body = req.body;
        const v = new Validator(body, StationAddRules);

        if (v.fails()) {
            return res.send(errors.get(128, v.errors.all()));
        }

        const stationId = body.stationId;

        if (!stationId) {
            return res.send(errors.missingParameter('stationId'));
        }

        try {
            const station = await Station.findById(stationId);

            if (!station) {
                return res.send(errors.get(122));
            }

            const source = pick(body, Object.keys(StationAddRules));

            Object.assign(station, source);
            await station.save();

            return res.send(errors.noError());
        } catch (err) {
            return res.send(errors.get(128, [err.message]));
        }
    });


    router.post('/station/remove', async function (req, res) {
        const body = req.body;
        const stationId = body.stationId;

        if (!stationId) {
            return res.send(errors.missingParameter('stationId'));
        }

        try {
            const station = await Station.findById(stationId);

            if (!station) {
                return res.send(errors.get(122));
            }

            await station.remove();

            return res.send(errors.noError());
        } catch (err) {
            return res.send(errors.get(128, [err.message]));
        }
    });


    router.post('/station/all', async function (req, res) {
        try {
            const stations = await Station.find({user: req.user._id});

            return res.send(errors.noError(stations));
        } catch (err) {
            return res.send(errors.get(120, [err.message]));
        }
    });

    router.post('/inverter/add', async function (req, res) {
        const body = req.body;
        const v = new Validator(body, WIFIInverterAddRules);

        if (v.fails()) {
            return res.send(errors.get(121, v.errors.all()));
        }

        const stationId = body.stationId;

        if (!stationId) {
            return res.send(errors.missingParameter('stationId'));
        }

        try {
            const station = await Station.findById(stationId);

            if (!station) {
                return res.send(errors.get(122));
            }

            const source = pick(body, Object.keys(WIFIInverterAddRules));

            try {
                await $okInverter(station, source);
            } catch (err) {
                return res.send(err);
            }

            station.wifiInverters.push(source);
            station.markModified('wifiInverters');

            await station.save();

            return res.send(errors.noError());
        } catch (err) {
            return res.send(errors.get(121, [err.message]));
        }
    });

    router.post('/inverter/update', async function (req, res) {
        const body = req.body;
        const v = new Validator(body, WIFIInverterAddRules);

        if (v.fails()) {
            return res.send(errors.get(126, v.errors.all()));
        }

        const stationId = body.stationId;

        if (!stationId) {
            return res.send(errors.missingParameter('stationId'));
        }

        const inverterId = body.inverterId;

        if (!inverterId) {
            return res.send(errors.missingParameter('inverterId'));
        }


        try {
            const station = await Station.findById(stationId);
            const source = pick(body, Object.keys(WIFIInverterAddRules));

            if (!station) {
                return res.send(errors.get(122));
            }

            const exists = station.wifiInverters.find(w => w._id.equals(inverterId));

            if (!exists) {
                return res.send(errors.get(130));
            }

            Object.assign(exists, source);
            station.markModified('wifiInverters');

            await station.save();

            return res.send(errors.noError());
        } catch (err) {
            return res.send(errors.get(126, [err.message]));
        }
    });

    router.post('/inverter/remove', async function (req, res) {
        const body = req.body;

        const stationId = body.stationId;

        if (!stationId) {
            return res.send(errors.missingParameter('stationId'));
        }

        const inverterId = body.inverterId;

        if (!inverterId) {
            return res.send(errors.missingParameter('inverterId'));
        }

        try {
            const station = await Station.findById(stationId);

            if (!station) {
                return res.send(errors.get(122));
            }

            const exists = station.wifiInverters.find(w => w._id.equals(inverterId));

            if (!exists) {
                return res.send(errors.get(130));
            }

            station.wifiInverters.pull({_id: inverterId});
            station.markModified('wifiInverters');

            await station.save();

            return res.send(errors.noError());
        } catch (err) {
            return res.send(errors.get(127, [err.message]));
        }
    });

    router.post('/user/info', function (req, res) {
        UserController.get(req.user._id, function (err, user) {
            res.send(errors.response(err, user, 122));
        });
    });

    router.post('/user', function (req, res) {
        if (!req.user || !req.user._id) {
            return res.send(errors.get(148));
        }

        UserController.get(req.user._id, (err, user) => {
            res.send(errors.response(err, user, 151));
        })
    });

    router.post('/user/update', function (req, res) {
        const body = req.body;

        UserController.update(req.user._id, body, oneUpdatedCallback(err => {
            res.send(errors.response(err, null, 149));
        }, res))
    });


    router.post('/config/set', async function (req, res) {
        const body = req.body;
        const v = new Validator(body, MqttConfigAddRules);

        if (v.fails()) {
            return res.send(errors.get(133, v.errors.all()));
        }

        try {
            const source = pick(body, Object.keys(MqttConfigAddRules));
            source.ip = allConfigs.ip;
            source.port = allConfigs.port;

            const last = await MqttConfig.findOne({status: ConfigStatus.LIVE});

            if (!source.source) {
                if (last && last.source) {
                    source.source = last.source;
                }
            }

            if (source.source && !source.source.startsWith("/")) {
                source.source = "/" + source.source;
            }

            ['version', 'minorVersion', 'buildVersion'].forEach(p=>{
                source[p] = source[p] || last[p];

                if(source[p] < last[p]){
                    throw new Error(p + " can not be less than previous");
                }
            });

            const config = new MqttConfig(source);
            await config.save();
            await MqttConfig.update({_id: {$ne: config._id}}, {$set: {status: ConfigStatus.OLD}}, {multi: true});

            return res.send(errors.noError());
        } catch (err) {
            return res.send(errors.get(133, [err.message]));
        }
    });

    router.get('/chart/excel', async function (req, res) {
        try {
            const query = req.query;
            query.properties = Array.isArray(query.properties) ? query.properties : [query.properties];
            query.inverters = Array.isArray(query.inverters) ? query.inverters : [query.inverters];
            const data = await getChartData(query);
            const columns = [...data.series.map(d => {
                return {header: d, key: d}
            }), {header: "Time", key: "Time"}];
            const d = zip([...data.data, data.time]);
            const workbook = excel.create(columns, d);

            await excel.stream(workbook, res)
        } catch (err) {
            res.send(errors.get(132, [err]))
        }
    });

};
